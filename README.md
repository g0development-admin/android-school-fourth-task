## Fourth task: Google maps

**Technologies used:**

1. Google maps
2. Retrofit
3. Hilt
4. Coroutines - Flow

<img alt="ImageGalleryDemo" src="https://github.com/aleh-god/fourth-task-maps-learn/blob/master/FourthTaskDemo.gif" />